#include <proto/conf_decrypt.h>

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdbool.h>

#include <proto/log.h>

#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>

static const char *hardcoded_key = "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1cMiFURVoBuIWn2UeLUx\niWevQfs9Ie32g977a9xMXOE9lCgQpC5b1m9s8BT9s+dI/Zlg6ZsI9SBLUeqoJg/u\nL3yBxRPUGohaF4/d8AYnO2+gYx4h+L6YXtfq72Ju24bD7bm5/i8miAIfcKw62iIx\nhNwBdQHatz9bvz4wKbBiXu/94iMfd1fG5CP6eMKies2YAxcBM3KAU/dafcQrHwpU\nAQQM9SbXrabsoX8sX6C1r9K6ritHi9lbT5rFb9vBUSDtNvu0BoH4HD2vLjJIbTIX\nBiTNTwsUnMFwbN+JnbZTF3ekhnsj3lX+tiK4Zpzi5Fnu6ndgWdZPsu74VglQ0433\niQIDAQAB\n-----END PUBLIC KEY-----\n";

/************ simple base64 lib *************/
/* http://web.mit.edu/freebsd/head/contrib/wpa/src/utils/base64.c */
static const unsigned char base64_table[65]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
static unsigned char*base64_decode(const unsigned char*src,size_t len,size_t*out_len){unsigned char dtable[256],*out,*pos,block[4],tmp;
size_t i,count,olen;int pad=0;if(len==0){out_len=0;out=malloc(1);out[0]='\0';return out;}memset(dtable,0x80,256);for(i=0;i<sizeof
(base64_table)-1;i++)dtable[base64_table[i]]=(unsigned char)i;dtable['=']=0;count=0;for(i=0;i<len;i++){if(dtable[src[i]]!=0x80)
count++;}if(count==0||count%4)return NULL;olen=count/4*3;pos=out=malloc(olen);if(out==NULL)return NULL;count=0;for(i=0;i<len;i++)
{tmp=dtable[src[i]];if(tmp==0x80)continue;if(src[i]=='=')pad++;block[count]=tmp;count++;if(count==4){*pos++=(block[0]<<2)|(block[
1]>>4);*pos++=(block[1]<<4)|(block[2]>>2);*pos++=(block[2]<<6)|block[3];count=0;if(pad){if(pad==1)pos--;else if(pad==2)pos-=2
;else{free(out);return NULL;}break;}}}*out_len=pos-out;return out;}

/************ simple base64 lib end *************/

static bool cstr_startswith(const char *tested_str, const char *niddle) {
    int tested_len = strlen(tested_str);
    int niddle_len = strlen(niddle);
    if(tested_len < niddle_len) {
        return false;
    }
    return 0 == memcmp(tested_str, niddle, niddle_len);
}
static char * cstr_append(char *target, const char *to_append) {
    /* target will be free-ed, and return a new malloc-ed ptr. */
    char * new_str ;
    size_t target_len = 0;
    target_len = strlen(target);

    if((new_str = realloc(target, target_len+strlen(to_append)+1)) == NULL) {
        ha_alert("Insufficent memory...");
        return NULL; /* Should BOOM */
    }
    strcat(new_str + target_len,to_append);
    return new_str;
}

static RSA * createRSA(const char *pem, int isPublic)
{
    RSA *rsa = NULL;
    FILE *pFile = fmemopen((void*)pem, strlen(pem), "r");
    if(isPublic)
        rsa = PEM_read_RSA_PUBKEY(pFile, &rsa,NULL, NULL);
    else
        rsa = PEM_read_RSAPrivateKey(pFile, &rsa,NULL, NULL);
    return rsa;
}

static void do_decrypt(void *key, size_t key_len, void *data, size_t data_len, void **output_data, size_t *output_data_len, const char *method) {
    /* do_decrypt should free key_data & config_data if needed. */
    /* method is always rsa */
    free(key);
    RSA *pubKey = createRSA(hardcoded_key, 1);
    if(pubKey == NULL)
        goto do_dec_err;

    *output_data = malloc(data_len);
    if(*output_data == NULL)
        goto do_dec_err;
    
    int cipherBlockSize = RSA_size(pubKey);
    /* int plainBlockSize = RSA_size(pubKey) - 11; */
    int plainIndex = 0;
    for(int index = 0; index < data_len; index += cipherBlockSize) {
        int flen = index + cipherBlockSize > data_len ? data_len - index : cipherBlockSize;
        int res = RSA_public_decrypt(flen, (unsigned char *)data + index, (unsigned char *)*output_data + plainIndex, pubKey, RSA_PKCS1_PADDING);
        if(res == -1) {
            ha_alert("RSA_public_decrypt failed. ERR_get_error() = %ld. ", ERR_get_error());
            free(*output_data);
            *output_data = NULL;
            goto do_dec_err;
        }
        plainIndex += res;
    }
    *output_data_len = plainIndex;

do_dec_err:
    free(data);
}

FILE *decrypt_config_file(const char *filename) {
    FILE *f;f = fopen(filename, "r");
    if(f == NULL) {
        return NULL;
    }

    char *err_reason = NULL;

    bool is_encrypted = false;
    char *encrypt_method = NULL;
    char *key_string = malloc(1); /* Without endline */
    char *config_string = malloc(1);
    key_string[0] = '\0';
    config_string[0] = '\0';

    ssize_t line_size;
    size_t len;
    char *line = NULL;

    /* Read key & config */
    char *current_appending_str = NULL;
    while ((line_size = getline(&line, &len, f)) != -1) {
        if(line_size == 0 || line[0] == '#')
            continue;
        if(line[0] == '$') {
            /* encryption cmd. */
            if(cstr_startswith(line, "$ENCRYPTED WITH ")) {
                if(encrypt_method != NULL) {
                    err_reason = "duplicate '$ENCRYPTED WITH' command.";
                    goto parse_error;
                }
                if(cstr_startswith(line, "$ENCRYPTED WITH RSA")) {
                    is_encrypted = true;
                    encrypt_method = "RSA";
                }
                else {
                    err_reason = "unknown '$ENCRYPTED WITH' method. Only supporting RSA.";
                    goto parse_error;
                }
            }
            else if(cstr_startswith(line, "$BEGIN KEY")) {
                is_encrypted = true;
                current_appending_str = key_string;
            }
            else if(cstr_startswith(line, "$BEGIN CONFIG")) {
                is_encrypted = true;
                current_appending_str = config_string;
            }
            else if(cstr_startswith(line, "$END KEY")) {
                if(current_appending_str == NULL) {
                    err_reason = "unexpected command '$END KEY'";
                    goto parse_error;
                }
                else {
                    key_string = current_appending_str;
                    current_appending_str = NULL;
                }
            }
            else if(cstr_startswith(line, "$END CONFIG")) {
                if(current_appending_str == NULL) {
                    err_reason = "unexpected command '$END CONFIG'";
                    goto parse_error;
                }
                else {
                    config_string = current_appending_str;
                    current_appending_str = NULL;
                }
            }
            else {
                err_reason = "unknown command beginning with $.";
                goto parse_error;
            }
            continue;
        }
        if(current_appending_str != NULL) {
            size_t line_len = strlen(line);
            if(line[line_len-1] == '\n' || line[line_len-1] == '\r') 
                line[line_len-1] = '\0';
            current_appending_str = cstr_append(current_appending_str, line);
            continue;
        }
    }
    if(current_appending_str != NULL) {
        err_reason = "Seen $BEGIN but doesn't see $END";
        goto parse_error;
    }
    if(line) {
        free(line);
        line = NULL;
    }
    fclose(f);
 
    /* Read key & config done. */

    if(is_encrypted) {
        /* base64 decode. */
        size_t key_data_len;
        unsigned char *key_data = base64_decode((unsigned char *)key_string, strlen(key_string), &key_data_len);
        if(key_data == NULL) {
            err_reason = "base64 decode failed.";
            goto decode_error;
        }
        size_t config_data_len;
        unsigned char *config_data = base64_decode((unsigned char *)config_string, strlen(config_string), &config_data_len);
        if(config_data == NULL) {
            err_reason = "base64 decode failed.";
            goto decode_error;
        }

        free(key_string); free(config_string);

        /* decrypt data. */
        void *decrypted_data = NULL;
        size_t decrypted_len = 0;
        /* do_decrypt should free key_data & config_data if needed. */
        do_decrypt(key_data, key_data_len, config_data, config_data_len, &decrypted_data, &decrypted_len, encrypt_method);

        if(decrypted_data == NULL) {
            err_reason = "decrypt failed.";
            goto decrypt_error;
        }
        /* printf("RECOLIC DEBUG: CONF: %s\n",(char*)decrypted_data); */

        /* return */
        return fmemopen(decrypted_data, decrypted_len, "r");
    }
    else {
        free(key_string); free(config_string);
        return fopen(filename, "r");
    }


parse_error:
    if(line)
        free(line);
    fclose(f);
decode_error:
    free(key_string); free(config_string);
decrypt_error:

    if(err_reason != NULL) {
        if(line != NULL)
            ha_alert("decrypt configuration file: %s at line:%s.\n", err_reason, line);
        else
            ha_alert("decrypt configuration file: %s.\n", err_reason);
    }
 
    return NULL;
}

