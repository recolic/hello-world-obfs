#!/bin/bash

git_archive_url="https://cdn.example.com/haproxy.tar.gz"
conf_url="https://cdn.example.com/encrypted.cfg"

function naive_check_os () {
    [[ -e /etc/fedora-release ]] && echo 'fedora' && return 0
    if [[ -e /usr/bin/lsb_release ]]; then
        lsb_release -a | grep 'Ubuntu' > /dev/null 2>&1 && echo 'ubuntu' && return 0
        lsb_release -a | grep 'Debian' > /dev/null 2>&1 && echo 'debian' && return 0
    fi
    [[ -e /usr/bin/yum ]] && yum --version | grep -F 'centos.org' > /dev/null 2>&1 && echo 'centos' && return 0
    uname -r | grep -F '-ARCH' > /dev/null 2>&1 && echo 'archlinux' && return 0
    echo 'unsupported' && return 1
}

os=$(naive_check_os)
cores=$(grep '^cpu cores' /proc/cpuinfo | uniq | awk '{print $4}')

# prepare
[[ $os = 'ubuntu' ]] || [[ $os = 'debian' ]] && apt update && apt install -y gcc make libssl-dev openssl wget
[[ $os = 'centos' ]] && yum install -y openssl-devel wget make centos-release-scl
[[ $os = 'archlinux' ]] && pacman -Sy --noconfirm gcc openssl make wget
[[ $os = 'fedora' ]] && dnf install -y gcc openssl openssl-devel make wget
if [[ $os = 'centos' ]]; then
    yum install -y devtoolset-7-gcc-c++ &&
    export HAPROXY_CC=/opt/rh/devtoolset-7/root/usr/bin/cc && export HAPROXY_CXX=/opt/rh/devtoolset-7/root/usr/bin/c++ || # Providing gcc6/7/8 here is OK.
    exit 2
fi

function do_build () {
    [[ $os = 'centos' ]] && 
        make -j "$cores" TARGET=linux-glibc USE_OPENSSL=1 CC="$HAPROXY_CC" && return 0
        # gcc in CentOS is outdated.
    [[ $os != 'centos' ]] &&
        make -j "$cores" TARGET=linux-glibc USE_OPENSSL=1
    return $?
}

rm -rf haproxy-2.0
wget "$git_archive_url" -O haproxy-plus.tar.gz &&
    tar xvzf haproxy-plus.tar.gz &&
    cd haproxy-2.0 &&
    do_build &&
    mv ./haproxy /usr/bin/ &&
    cd .. && rm -rf haproxy-2.0 &&
    echo haproxy installtion done. ||
    exit $?

wget "$conf_url" -O haproxy.conf &&
    mv haproxy.conf /etc/haproxy.conf &&
    echo configuration installation done. ||
    exit $?

echo '
[Unit]
Description=HAProxy Service

[Service]
TimeoutStartSec=0
ExecStart=/usr/bin/haproxy -f /etc/haproxy.conf

[Install]
WantedBy=multi-user.target
' > /tmp/haproxy.m.service &&
    mv /tmp/haproxy.m.service /etc/systemd/system/ &&
    systemctl enable haproxy.m.service --now &&
    sleep 1 &&
    ps aux | grep -v grep | grep haproxy &&
    echo Systemd setup done. ||
    exit $?

