#include <iostream>

#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <cstdio>

#include <string>
#include <cstring>
#include "rlib.min.hpp"

// NOTE: WARNING: I didn't free any malloc-ed memory in this simple tool.
//   It works. And I think that's enough.

// Usage: ./rsa_enc < plain.conf > encrypted.conf

static const char *hardcoded_key_pub = "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1cMiFURVoBuIWn2UeLUx\niWevQfs9Ie32g977a9xMXOE9lCgQpC5b1m9s8BT9s+dI/Zlg6ZsI9SBLUeqoJg/u\nL3yBxRPUGohaF4/d8AYnO2+gYx4h+L6YXtfq72Ju24bD7bm5/i8miAIfcKw62iIx\nhNwBdQHatz9bvz4wKbBiXu/94iMfd1fG5CP6eMKies2YAxcBM3KAU/dafcQrHwpU\nAQQM9SbXrabsoX8sX6C1r9K6ritHi9lbT5rFb9vBUSDtNvu0BoH4HD2vLjJIbTIX\nBiTNTwsUnMFwbN+JnbZTF3ekhnsj3lX+tiK4Zpzi5Fnu6ndgWdZPsu74VglQ0433\niQIDAQAB\n-----END PUBLIC KEY-----\n";
const char *hardcoded_key = "-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEA1cMiFURVoBuIWn2UeLUxiWevQfs9Ie32g977a9xMXOE9lCgQ\npC5b1m9s8BT9s+dI/Zlg6ZsI9SBLUeqoJg/uL3yBxRPUGohaF4/d8AYnO2+gYx4h\n+L6YXtfq72Ju24bD7bm5/i8miAIfcKw62iIxhNwBdQHatz9bvz4wKbBiXu/94iMf\nd1fG5CP6eMKies2YAxcBM3KAU/dafcQrHwpUAQQM9SbXrabsoX8sX6C1r9K6ritH\ni9lbT5rFb9vBUSDtNvu0BoH4HD2vLjJIbTIXBiTNTwsUnMFwbN+JnbZTF3ekhnsj\n3lX+tiK4Zpzi5Fnu6ndgWdZPsu74VglQ0433iQIDAQABAoIBAAdSkOdTmO9MIiaf\naKed5V2EnL9Q4O45nZk02T3/pOoFmTtrB+7n26OD26J5xZsVEC4HxAcclXj4Kzxa\nInpzT0B3LmbW4AwsoZFvDzY6r7tcfo0GqjlAJi4RjW233H5OpoWNOC7Z5KMCF5uf\nesCgLTwU4UPl1V0zoWvysGmtLiF8XRQ28l/0FXWe2bFqNElQjsT8p2SMAuZRNfpg\nds3HAd361pb8MyRwUCXzXYUC9S2T61Y3oaopLtMNO4hVzDKb6m8YYO4tbAfmbvYX\nklvS495UVG78U8ylUwzaHWdrGmbIJ24BkowMhOfKpgDZE/h5+FB35MxUDzG6TuHr\nOlAOgBECgYEA9t+AeGHpHnV0mWK00FdEx++NHSQk8aAVD8uCk5JDLjLq6UIA1kbo\nVuhjcLkWSEAs8mqdXx/K7Ge+zLs1zv56TFrS20qApovJREvBKLIWRGskLWk0WcKA\njf2UlQFEDxq/KAIzsBuFZRpFWu6WLc/mT48KyWr2Na5sYxZaKsdEaeUCgYEA3apC\nHXWfTVrsH0KJsVpJhBHHpwe7FykYbES8YgqwnCvAW7WRLiSN8M3NrpunQXkZ4DHi\nE4yvyf1nl9QVS1W3gASMR2Uv7wEtu+aZqur5MTnwdY5ndcCyRCOr2lK5cQrpRIJX\nIBr0EaXzSFEErOuioI+9oqcqgSBGwtXZxRbMrNUCgYEAlCZCMXwIasfvxhCvE4wb\ncaR3WbKUHRCFMypc3PRcLoBN6tc6K1z4Fxd0fXQFRCyHDJ8ge7JXk5Eatj/upZ6f\ng+zJNuI53Dhs+vg/HY3pwPtPBZVp3ifT/+HaZaUR08KoTnkrzZZmcerXDh+5rhpi\nSwI7Ze4zfLmEv93tFFjq3GUCgYBGmThve4veLkwv9Jg4NUJe3u7QwwSSbtNJgGbj\nbvYq90BPtTdlE91lSZbtusN9dd2I6zpO8ar3UU8SK0kQ8l78VfZ7sr2eiKiRRPPC\nXQ464HI2bsGC3nmofBf69Af9+fSrjjvDjPIZQNqIOM2daXslWkKSsF04PRhsa4Ht\nZMKRdQKBgC8Fjk3yGoC6jvVtA9Yb3uxWR3teoB832zROi8P0oPsMzoIh4nJQPuzw\nL0g55yy3GASGNbHYXa3vy+bVJkOUz9X+2J6siqKO5ornduqZ6O+ZSwTtokPGclXu\nAYgipUzR79Dra5j6dWOlrM96v8TLSNxCmTJouDDLbJKCKUbrWd3G\n-----END RSA PRIVATE KEY-----\n";
static RSA * createRSA(const char *pem, int isPublic)
{
    RSA *rsa = NULL;
    FILE *pFile = fmemopen((void *)pem, strlen(pem), "r");
    if(isPublic)
        rsa = PEM_read_RSA_PUBKEY(pFile, &rsa,NULL, NULL);
    else
        rsa = PEM_read_RSAPrivateKey(pFile, &rsa, NULL, NULL);
    return rsa;
}


static void do_encrypt(void *data, size_t data_len, void **output_data, size_t *output_data_len, const char *method) {
    /* method is always rsa */
    RSA *pubKey = createRSA(hardcoded_key, 0);
    if(pubKey == NULL)
        throw std::runtime_error("invalid key");

    *output_data = malloc(data_len + RSA_size(pubKey));
    if(*output_data == NULL)
        throw std::runtime_error("malloc failed");
    
    int cipherBlockSize = RSA_size(pubKey) - 11; // cipher means plain. just copy-pasting fro mdecrypt.
    /* int plainBlockSize = RSA_size(pubKey); */
    int plainIndex = 0;
    for(int index = 0; index < data_len; index += cipherBlockSize) {
        int flen = index + cipherBlockSize > data_len ? data_len - index : cipherBlockSize;
        int res = RSA_private_encrypt(flen, (unsigned char *)data + index, (unsigned char *)*output_data + plainIndex, pubKey, RSA_PKCS1_PADDING);
        if(res == -1) {
            printf("RSA_private_encrypt failed. ERR_get_error() = %ld. ", ERR_get_error());
            *output_data = NULL;
            throw std::runtime_error("encrypt failed");
        }
        plainIndex += res;
    }
    *output_data_len = plainIndex;

}


static void do_decrypt(void *data, size_t data_len, void **output_data, size_t *output_data_len, const char *method) {
    /* method is always rsa */
    RSA *pubKey = createRSA(hardcoded_key_pub, 1);
    if(pubKey == NULL)
        throw std::runtime_error("invalid public key");

    *output_data = malloc(data_len + RSA_size(pubKey));
    if(*output_data == NULL)
        throw std::runtime_error("sucks");
    
    int cipherBlockSize = RSA_size(pubKey);
    /* int plainBlockSize = RSA_size(pubKey) - 11; */
    int plainIndex = 0;
    for(int index = 0; index < data_len; index += cipherBlockSize) {
        int flen = index + cipherBlockSize > data_len ? data_len - index : cipherBlockSize;
        int res = RSA_public_decrypt(flen, (unsigned char *)data + index, (unsigned char *)*output_data + plainIndex, pubKey, RSA_PKCS1_PADDING);
        if(res == -1) {
            printf("RSA_private_encrypt failed. ERR_get_error() = %ld. ", ERR_get_error());
            *output_data = NULL;
        throw std::runtime_error("sucks");
        }
        plainIndex += res;
    }
    *output_data_len = plainIndex;

}
static const unsigned char base64_table[65]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
unsigned char*base64_encode(const unsigned char*src,size_t len,size_t*out_len){unsigned char*out,*pos;
const unsigned char*end,*in;size_t olen;int line_len;olen=len*4/3+4;olen+=olen/72;olen++;if(olen<len)return NULL;
out=(unsigned char*)malloc(olen);if(out==NULL)return NULL;end=src+len;in=src;pos=out;line_len=0;while(end-in>=3){*pos++=base64_table[in[0]>>2];
*pos++=base64_table[((in[0]&0x03)<<4)|(in[1]>>4)];*pos++=base64_table[((in[1]&0x0f)<<2)|(in[2]>>6)];*pos++=
base64_table[in[2]&0x3f];in+=3;line_len+=4;if(line_len>=72){*pos++='\n';line_len=0;}}if(end-in){*pos++=base64_table
[in[0]>>2];if(end-in==1){*pos++=base64_table[(in[0]&0x03)<<4];*pos++='=';}else{*pos++=base64_table[((in[0]&0x03)
<<4)|(in[1]>>4)];*pos++=base64_table[(in[1]&0x0f)<<2];}*pos++='=';line_len+=4;}if(line_len)*pos++='\n';*pos='\0';
if(out_len)*out_len=pos-out;return out;}

/*OpenSSL_add_all_algorithms();
OpenSSL_add_all_ciphers();
ERR_load_crypto_strings();*/

using namespace rlib;
int main() {
    rlib::string plain;
    while(!std::cin.eof()) {
        plain += scanln() + '\n';
    }

#ifdef TESTING
    println("---------- PLAIN ---------");
    println(plain);
    println("---------- PLAIN END ---------");
#endif

    void *output_data = nullptr;
    size_t output_len = 0;
    do_encrypt((void *)plain.data(), plain.size(), &output_data, &output_len, "rsa");
#ifdef TESTING
    println("---------- ENC ---------");
    println((char *)output_data);
    println("---------- ENC END ---------");
#endif


#ifdef TESTING
    void *p2 = nullptr;
    size_t s2 = 0;
    do_decrypt(output_data, output_len, &p2, &s2, "rsa");
    ((char *)p2)[s2 ]= 0;
    println("---------- PLAIN ---------");
    println((char *)p2);
    println("---------- PLAIN END ---------");
#endif

    size_t base64_len = 0;
    const char *b = (char *)base64_encode((unsigned char *)output_data, output_len, &base64_len);
    std::string bstr(b);
    println("$BEGIN CONFIG");
    println(bstr);
    println("$END CONFIG");


}


