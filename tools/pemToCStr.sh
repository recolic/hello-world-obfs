#!/bin/bash

[[ "$1" = '' ]] && echo "Usage: $0 <filename.pem>" && exit 1
echo -n 'const char *hardcoded_key = "'
cat "$1" | tr '\n' '@' | sed 's/@/\\n/g'
echo -n '";'



